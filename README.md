# Outbox Pattern CDC

## References
- An [article](https://medium.com/@cooper.wolfe/i-hated-debezium-so-much-i-did-it-myself-b43b0efc20a9) describing this repository
- [How to configure](configuration.md)

## Purpose
The purpose of this repository is to share a CDC tool I wrote from scratch using DotNet. This is pulled from a proprietary repository of another project I was working on (The Town Square App, unreleased at the time of writing) but has bits and bobs removed as needed, and has been trimmed down to the just the CDC tool and its configuration.

I wrote the tool as a response to disliking the complexities of configuring Debezium to suit my needs and found re-inventing the wheel to be more beneficial.

If you see something you like here, by all means please take it. I hope it makes your life easier and improves your application. However, if you could leave a reference back to me either by forking or linking my repository or [article](https://medium.com/@cooper.wolfe/i-hated-debezium-so-much-i-did-it-myself-b43b0efc20a9), I would appreciate it.

## Disclaimer
I left a lot to be desired in this implementation, and you shouldn't assume things are going to work on the first try without fully understanding the code. As I said before, I just needed to solve a problem in my own application with ease and I wasn't too concerned with how extensible it was. In addition, the format of the code is entirely unrelated to this repository. I'm not certain the amount of project decoupling would have been necessary if I had started the codebase here. If you have improvements, I can't guarantee I will be maintaining this repository long-term, but I'm open to suggestions and merge requests.

### Postgres
The Postgres implementation assumes you have a replication slot available on your Postgres instance. See [pg_recvlogical](https://www.postgresql.org/docs/current/app-pgrecvlogical.html) for more details.

In especially large applications, having too many open replication slots can cause serious performance problems. Using [foreign data wrappers](https://wiki.postgresql.org/wiki/Foreign_data_wrappers), you can ensure all databases on a single server use the same cdc service, and thus the same replication slot. While this couples your database to its server, as long as you have one CDC service per server, you shouldn't be increasing the coupling any more than would be natural otherwise.

### Mongo
The Mongo implementation assumes you have a Mongo database hosted in a replica set named r0. I could see the configuration of Mongo being changed to use a connection string instead of the existing environment variables quite easily, but ... oh well.
