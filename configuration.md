# Configuration
How to configure the cdc (Change Data Capture) service

---
## `SOURCE`
The data source to capture from

### `SOURCE=Postgres`

#### `POSTGRES_HOST`
The host of the source Postgres instance

#### `POSTGRES_PORT`
The port of the source Postgres instance

#### `POSTGRES_USER`
The user to use when authenticating with the source Postgres instance

#### `POSTGRES_PASSWORD`
The password to use when authenticating with the source Postgres instance

#### `POSTGRES_DATABASE`
The database of the source Postgres instance

#### `POSTGRES_SCHEMA`
The schema of the source table

#### `POSTGRES_TABLE`
The source table, not including schema

#### `POSTGRES_SLOT`
The name of the slot to use

#### `POSTGRES_PGPASS`
The location of .pgpass in the container

### `SOURCE=Neo4j`

#### `NEO4J_HOST`
The host of the source Neo4j instance

#### `NEO4J_PORT`
The port of the source Neo4j instance

#### `NEO4J_USER`
The user to use when authenticating with the source Neo4j instance

#### `NEO4J_PASSWORD`
The password to use when authenticating with the source Neo4j instance

#### `NEO4J_DATABASE`
The database of the source Neo4j instance

#### `NEO4J_OUTBOX_LABEL`
The label used on outbox nodes

#### `NEO4J_POLL_TIMEOUT_MS`
Neo4j does not come with CDC without creating a dependency on the message bus.
For this reason, the CDC service's implementation utilizes polling to accomplish CDC.
This variable configures the poll timeout.

### `SOURCE=Mongo`

#### `MONGO_HOST`
The host of the source Mongo instance

#### `MONGO_PORT`
The port of the source Mongo instance

#### `MONGO_USERNAME`
The user to use when authenticating with the source Mongo instance

#### `MONGO_PASSWORD`
The password to use when authenticating with the source Mongo instance

#### `MONGO_DATABASE`
The database of the source MONGO instance

#### `MONGO_OUTBOX_COLLECTION`
The name of the outbox collection, e.g. "outbox"

---
## `TARGET`
The target to send captured data to

### `TARGET=Kafka`

#### `KAFKA_BOOTSTRAP_SERVERS`
The connection string to the target Kafka bootstrap server(s) (comma-separated)

#### `KAFKA_SCHEMA_REGISTRY_URL`
The connection string to the target schema registry server

#### `KAFKA_TOPIC`
The topic you want to push data to. Use `{}` to mark where to put the aggregate
type. Default is `outbox.{}`.

#### `KAFKA_MESSAGE_MAX_BYTES`
The maximum number of bytes that can be produced in a single topic message.

---
## `STATE`
Configuration about where to store the Cdc State

### `STATE=Fs`

#### `FS_STATE_FILE`
The fully qualified file to store the state in. Defaults to `/cdc/state`.