FROM mcr.microsoft.com/dotnet/sdk:6.0 AS base
WORKDIR /src
COPY Cdc.Worker/Cdc.Worker.csproj Cdc.Worker/
COPY Cdc.Fs/Cdc.Fs.csproj Cdc.Fs/
COPY Cdc.Kafka/Cdc.Kafka.csproj Cdc.Kafka/
COPY Cdc.Postgres/Cdc.Postgres.csproj Cdc.Postgres/
COPY Cdc.Neo4j/Cdc.Neo4j.csproj Cdc.Neo4j/
COPY Cdc.Mongo/Cdc.Mongo.csproj Cdc.Mongo/
COPY Cdc.Service/Cdc.Service.csproj Cdc.Service/
RUN dotnet restore Cdc.Worker
COPY Cdc.Worker/ Cdc.Worker/
COPY Cdc.Fs/ Cdc.Fs/
COPY Cdc.Kafka/ Cdc.Kafka/
COPY Cdc.Postgres/ Cdc.Postgres/
COPY Cdc.Neo4j/ Cdc.Neo4j/
COPY Cdc.Mongo/ Cdc.Mongo/
COPY Cdc.Service/ Cdc.Service/
RUN mkdir /cdc


FROM base AS debug
RUN apt-get update
RUN apt-get install -y postgresql-client-13
WORKDIR /src/Cdc.Worker
RUN dotnet build -c Debug
ENTRYPOINT [ "dotnet", "bin/Debug/net6.0/Cdc.Worker.dll" ]


FROM base as build
WORKDIR /src
ARG BUILD_NUMBER
RUN dotnet build Cdc.Worker/ -c Release --no-restore -p:Version="${BUILD_NUMBER}"


FROM build AS publish
WORKDIR /src
RUN dotnet publish Cdc.Worker/ -c Release -o /app/publish --no-restore --no-build


FROM mcr.microsoft.com/dotnet/aspnet:6.0-alpine AS release
RUN apk add bash postgresql13-client
RUN addgroup -g 10001 -S cdc && adduser -u 10001 -S cdc -G cdc
USER cdc
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT [ "dotnet", "Cdc.Worker.dll" ]
