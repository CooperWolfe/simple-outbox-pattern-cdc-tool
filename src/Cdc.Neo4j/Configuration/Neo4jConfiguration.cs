namespace Cdc.Neo4j.Configuration;
public class Neo4jConfiguration
{
    public string? Username { get; set; }
    public string? Password { get; set; }
    public string? Database { get; set; }
    public string? Host { get; set; }
    public string? Port { get; set; }
    public string? OutboxLabel { get; set; }
    public int? PollTimeoutMs { get; set; }

    internal string? Uri => $"bolt://{Host}:{Port}";
}