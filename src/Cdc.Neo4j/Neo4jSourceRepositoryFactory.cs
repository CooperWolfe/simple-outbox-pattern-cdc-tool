using Cdc.Neo4j.Configuration;
using Microsoft.Extensions.Options;
using Neo4j.Driver;

namespace Cdc.Neo4j;
internal class Neo4jSourceRepositoryFactory
{
    private readonly IDriver driver;
    private readonly IOptionsMonitor<Neo4jConfiguration> configuration;

    public Neo4jSourceRepositoryFactory(
        IDriver driver,
        IOptionsMonitor<Neo4jConfiguration> configuration)
    {
        this.driver = driver;
        this.configuration = configuration;
    }

    public Neo4jSourceRepository Create()
    {
        return new Neo4jSourceRepository(driver, configuration);
    }
}