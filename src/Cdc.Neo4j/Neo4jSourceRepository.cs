using Cdc.Neo4j.Configuration;
using Cdc.Service;
using Cdc.Service.State;
using Microsoft.Extensions.Options;
using Neo4j.Driver;
using Neo4j.Driver.Extensions;

namespace Cdc.Neo4j;
internal class Neo4jSourceRepository : ISourceRepository
{
    private readonly IAsyncSession session;
    private readonly Neo4jConfiguration configuration;

    public Neo4jSourceRepository(
        IDriver driver,
        IOptionsMonitor<Neo4jConfiguration> configuration)
    {
        this.session = driver.AsyncSession(cfg => cfg.WithDatabase(configuration.CurrentValue.Database));
        this.configuration = configuration.CurrentValue;
    }

    public async Task<IEnumerable<IChange>> ReadAllPending(LastConsumedState? lastConsumed, CancellationToken stoppingToken)
    {
        return await session.ReadTransactionAsync(async transaction =>
        {
            var query = lastConsumed == null
                ? new Query($@"
                    match (n:{configuration.OutboxLabel})
                    with n
                        order by n.createdAt
                    return n")
                : new Query($@"
                    match (n:{configuration.OutboxLabel})
                    where n.createdAt > $lastTimestamp
                    with n
                        order by n.createdAt
                    return n",
                    new { lastTimestamp = lastConsumed.CreatedAt });
            var result = await Task.Run(() => transaction.RunAsync(query), stoppingToken);

            return await Task.Run(() => result.ToListAsync(result =>
            {
                var node = result["n"].As<INode>();
                var change = node.ToObject<Neo4jChange>();
                return change;
            }), stoppingToken);
        });
    }
}
