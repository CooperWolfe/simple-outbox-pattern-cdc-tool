using Cdc.Service;
using Neo4j.Driver;
using Neo4j.Driver.Extensions;

namespace Cdc.Neo4j;
public class Neo4jChange : IChange
{
    [Obsolete("For Neo4j deserialization only")]
    public Neo4jChange()
    {
        Id = string.Empty;
        CreatedAtZoned = new ZonedDateTime(default);
        AggregateId = string.Empty;
        Payload = new byte[0];
        EventType = string.Empty;
        AggregateType = string.Empty;
    }

    [Neo4jProperty(Name = "id")]
    public string Id { get; set; }

    [Neo4jProperty(Name = "createdAt")]
    public ZonedDateTime CreatedAtZoned { get; set; }
    public DateTime CreatedAt => CreatedAtZoned.ToDateTimeOffset().UtcDateTime;

    [Neo4jProperty(Name = "aggregateId")]
    public string AggregateId { get; set; }

    [Neo4jProperty(Name = "payload")]
    public byte[] Payload { get; set; }

    [Neo4jProperty(Name = "eventType")]
    public string EventType { get; set; }

    [Neo4jProperty(Name = "aggregateType")]
    public string AggregateType { get; set; }
}
