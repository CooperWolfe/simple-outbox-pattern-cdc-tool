using Cdc.Neo4j.Configuration;
using Cdc.Service.Listener;
using Microsoft.Extensions.Options;

namespace Cdc.Neo4j;
internal class Neo4jSourceListenerFactory : ISourceListenerFactory
{
    private readonly IOptionsMonitor<Neo4jConfiguration> options;
    private readonly Neo4jSourceRepositoryFactory repositoryFactory;

    public Neo4jSourceListenerFactory(
        IOptionsMonitor<Neo4jConfiguration> options,
        Neo4jSourceRepositoryFactory repositoryFactory)
    {
        this.options = options;
        this.repositoryFactory = repositoryFactory;
    }

    public ISourceListener Create(ISourceListenerDelegate @delegate)
    {
        return new Neo4jSourceListener(
            @delegate,
            options.CurrentValue,
            repositoryFactory);
    }
}
