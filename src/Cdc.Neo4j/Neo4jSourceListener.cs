using Cdc.Neo4j.Configuration;
using Cdc.Service.Listener;
using Cdc.Service.State;

namespace Cdc.Neo4j;
internal class Neo4jSourceListener : ISourceListener
{
    private bool hasCalledListen = false;

    private readonly ISourceListenerDelegate @delegate;
    private readonly Neo4jConfiguration configuration;
    private readonly Neo4jSourceRepositoryFactory repositoryFactory;

    public Neo4jSourceListener(
        ISourceListenerDelegate @delegate,
        Neo4jConfiguration configuration,
        Neo4jSourceRepositoryFactory repositoryFactory)
    {
        this.@delegate = @delegate;
        this.configuration = configuration;
        this.repositoryFactory = repositoryFactory;
    }

    public async Task Listen(LastConsumedState? lastConsumed, CancellationToken cancellationToken)
    {
        hasCalledListen = hasCalledListen ? throw new InvalidOperationException() : true;

        while (!cancellationToken.IsCancellationRequested)
        {
            var repository = repositoryFactory.Create();
            var changes = await repository.ReadAllPending(lastConsumed, cancellationToken);
            if (changes.Any())
            {
                lastConsumed = await @delegate.OnTrigger(changes, cancellationToken);
            }
            await Task.Delay(configuration.PollTimeoutMs!.Value, cancellationToken);
        }
    }
    
    public void Dispose()
    {
        // Nothing to do
    }
}
