using Cdc.Neo4j;
using Cdc.Neo4j.Configuration;
using Cdc.Service;
using Cdc.Service.Listener;
using Microsoft.Extensions.Options;
using Neo4j.Driver;

namespace Microsoft.Extensions.DependencyInjection;
public static class Neo4jServiceCollectionExtensions
{
    public static void AddNeo4jSource(this IServiceCollection services, Action<OptionsBuilder<Neo4jConfiguration>> configure)
    {
        var optionsBuilder = services.AddOptions<Neo4jConfiguration>();
        configure(optionsBuilder);

        services.AddTransient<IDriver>(sp =>
        {
            var options = sp.GetRequiredService<IOptionsMonitor<Neo4jConfiguration>>().CurrentValue;
            return GraphDatabase.Driver(options.Uri, AuthTokens.Basic(options.Username, options.Password));
        });
        services.AddTransient<ISourceRepository, Neo4jSourceRepository>();
        services.AddTransient<Neo4jSourceRepositoryFactory>();
        services.AddTransient<ISourceListenerFactory, Neo4jSourceListenerFactory>();
    }
}