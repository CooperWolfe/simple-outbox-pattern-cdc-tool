using Cdc.Mongo.Configuration;
using Cdc.Service;
using Cdc.Service.State;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Cdc.Mongo;
internal class MongoSourceRepository : ISourceRepository
{
    private readonly IMongoCollection<MongoChange> mongoCollection;

    public MongoSourceRepository(IOptions<MongoConfiguration> options)
    {
        var mongoClient = new MongoClient(options.Value.ConnectionString);
        var mongoDatabase = mongoClient.GetDatabase(options.Value.Database);
        mongoCollection = mongoDatabase.GetCollection<MongoChange>(options.Value.OutboxCollection);
    }

    public async Task<IEnumerable<IChange>> ReadAllPending(LastConsumedState? lastConsumed, CancellationToken cancellationToken)
    {
        if (lastConsumed == null)
        {
            var cursor = await mongoCollection.FindAsync(_ => true, cancellationToken: cancellationToken);
            return await cursor.ToListAsync(cancellationToken);
        }
        else
        {
            var cursor = await mongoCollection.FindAsync(entry =>
                entry.CreatedAt >= lastConsumed.CreatedAt
                && !lastConsumed.Ids.Contains(entry.Id),
                cancellationToken: cancellationToken);
            return await cursor.ToListAsync(cancellationToken);
        }
    }
}
