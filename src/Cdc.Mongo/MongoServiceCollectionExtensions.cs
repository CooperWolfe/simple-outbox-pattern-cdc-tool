﻿using Cdc.Mongo;
using Cdc.Mongo.Configuration;
using Cdc.Service;
using Cdc.Service.Listener;
using Microsoft.Extensions.Options;
using MongoDB.Bson.Serialization.Conventions;

namespace Microsoft.Extensions.DependencyInjection;
public static class MongoServiceCollectionExtensions
{
    public static void AddMongoSource(this IServiceCollection services, Action<OptionsBuilder<MongoConfiguration>> configure)
    {
        ConventionRegistry.Register(
            "camelCase",
            new ConventionPack
            {
                new CamelCaseElementNameConvention()
            },
            _ => true);

        var optionsBuilder = services.AddOptions<MongoConfiguration>();
        configure(optionsBuilder);
        services.AddLogging();

        services.AddTransient<ISourceRepository, MongoSourceRepository>();
        services.AddTransient<ISourceListenerFactory, MongoSourceListenerFactory>();
    }
}