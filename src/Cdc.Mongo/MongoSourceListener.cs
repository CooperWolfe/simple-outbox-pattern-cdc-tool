using Cdc.Mongo.Configuration;
using Cdc.Service.Listener;
using Cdc.Service.State;
using MongoDB.Driver;

namespace Cdc.Mongo;
internal class MongoSourceListener : ISourceListener
{
    private readonly ISourceListenerDelegate @delegate;
    private readonly IChangeStreamCursor<ChangeStreamDocument<MongoChange>> cursor;

    public MongoSourceListener(
        ISourceListenerDelegate @delegate,
        MongoConfiguration mongoConfiguration)
    {
        this.@delegate = @delegate;

        var mongoClient = new MongoClient(mongoConfiguration.ConnectionString);
        var localDatabase = mongoClient.GetDatabase(mongoConfiguration.Database);
        var outbox = localDatabase.GetCollection<MongoChange>(mongoConfiguration.OutboxCollection);
        cursor = outbox.Watch();
    }

    public async Task Listen(LastConsumedState? lastConsumedState, CancellationToken cancellationToken)
    {
        while (await cursor.MoveNextAsync(cancellationToken))
        {
            var captures = cursor.Current
                .Where(doc => doc.OperationType == ChangeStreamOperationType.Insert);
            if (lastConsumedState != null)
            {
                captures = captures.Where(capture =>
                    capture.FullDocument.CreatedAt >= lastConsumedState.CreatedAt
                    && !lastConsumedState.Ids.Contains(capture.FullDocument.Id));
            }

            if (captures.Any())
            {
                var changes = captures.Select(doc => doc.FullDocument);
                await @delegate.OnTrigger(changes, cancellationToken);
            }
        }
    }

    public void Dispose()
    {
        cursor.Dispose();
    }
}
