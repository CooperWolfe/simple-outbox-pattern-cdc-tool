using Cdc.Service;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Cdc.Mongo;
internal class MongoChange : IChange
{
    [BsonId]
    public ObjectId _Id { get; set; }
    public string Id { get; set; } = string.Empty;
    public string AggregateId { get; set; } = string.Empty;
    public string AggregateType { get; set; } = string.Empty;
    public byte[] Payload { get; set; } = new byte[0];
    public string EventType { get; set; } = string.Empty;
    public DateTime CreatedAt { get; set; }
}