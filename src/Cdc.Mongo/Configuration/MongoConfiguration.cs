namespace Cdc.Mongo.Configuration;
public class MongoConfiguration
{
    public string? Host { get; set; }
    public int? Port { get; set; }
    public string? Username { get; set; }
    public string? Password { get; set; }
    public string? Database { get; set; }
    public string? OutboxCollection { get; set; }

    public string ConnectionString => $"mongodb://{Username}:{Password}@{Host}:{Port}?connect=replicaSet&replicaSet=r0";
}