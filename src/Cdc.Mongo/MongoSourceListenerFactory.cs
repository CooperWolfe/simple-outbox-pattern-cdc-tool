using Cdc.Mongo.Configuration;
using Cdc.Service.Listener;
using Microsoft.Extensions.Options;

namespace Cdc.Mongo;
internal class MongoSourceListenerFactory : ISourceListenerFactory
{
    private readonly IOptions<MongoConfiguration> options;

    public MongoSourceListenerFactory(IOptions<MongoConfiguration> options)
    {
        this.options = options;
    }

    public ISourceListener Create(ISourceListenerDelegate @delegate)
    {
        return new MongoSourceListener(@delegate, options.Value);
    }
}
