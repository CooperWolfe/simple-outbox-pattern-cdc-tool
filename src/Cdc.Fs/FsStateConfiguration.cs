namespace Cdc.Fs
{
    internal class FsStateConfiguration
    {
        public string? File { get; set; } = "/cdc/state";
    }
}