using System;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Cdc.Service.Exceptions;
using Cdc.Service.State;
using Microsoft.Extensions.Options;

namespace Cdc.Fs
{
    internal class FsStateRepository : IStateRepository
    {
        private bool hasCachedState = false;
        private LastConsumedState? cachedState = null;

        private readonly FsStateConfiguration fsStateConfiguration;

        public FsStateRepository(IOptions<FsStateConfiguration> fsStateConfiguration)
        {
            this.fsStateConfiguration = fsStateConfiguration.Value;
        }

        public async Task<LastConsumedState?> LoadLastConsumed(CancellationToken stoppingToken)
        {
            StreamReader reader;
            try
            {
                reader = File.OpenText(fsStateConfiguration.File!);
            }
            catch (SystemException ex) when (
                   ex is UnauthorizedAccessException
                || ex is IOException && ex is not FileNotFoundException
                || ex is NotSupportedException)
            {
                throw new StateException("Failed to open state file", ex);
            }
            catch (FileNotFoundException)
            {
                return Cache(null);
            }

            using (reader)
            {
                string? line;
                try
                {
                    line = await Task.Run(reader.ReadLineAsync, stoppingToken);
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    // The only time this can occur naturally is if > 59,652,323 (int.MaxValue / 36 [the character length of a GUID]) events occur simultaneously
                    throw new StateException("State is either corrupted or an unexpectedly large number of events were published simultaneously", ex);
                }

                return string.IsNullOrWhiteSpace(line)
                    ? Cache(null)
                    : Cache(ParseState(line));
            }
        }

        public LastConsumedState? ReadCachedLastConsumed()
        {
            return hasCachedState ? cachedState : throw new InvalidOperationException();
        }

        public async Task UpdateLastConsumed(LastConsumedState lastConsumedState, CancellationToken stoppingToken)
        {
            StreamWriter writer;
            try
            {
                writer = File.CreateText(fsStateConfiguration.File!);
            }
            catch (SystemException ex) when (
                   ex is UnauthorizedAccessException
                || ex is IOException
                || ex is NotSupportedException)
            {
                throw new StateException("Failed to open state file", ex);
            }

            using (writer)
            {
                await writer.WriteLineAsync($"{lastConsumedState.CreatedAt:o} {string.Join(',', lastConsumedState.Ids)}");
            }

            Cache(lastConsumedState);
        }

        private LastConsumedState? Cache(LastConsumedState? state)
        {
            hasCachedState = true;
            return cachedState = state;
        }

        private static LastConsumedState ParseState(string line)
        {
            string[] tokens = line.Trim().Split(' ');
            if (tokens.Length != 2)
            {
                throw new StateException($"Unexpectedly found state with {tokens.Length} tokens. Expected 2: date and ids.");
            }

            if (!DateTime.TryParse(tokens[0], provider: null, styles: DateTimeStyles.RoundtripKind, out DateTime timestamp))
            {
                throw new StateException($"Failed to parse first token of state to a DateTime: {tokens[0]}");
            }

            return new LastConsumedState(
                Ids: tokens[1].Split(','),
                CreatedAt: timestamp);
        }
    }
}