using Cdc.Fs;
using Cdc.Service.State;
using Microsoft.Extensions.Configuration;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class FsServiceCollectionExtensions
    {
        public static void AddFs(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddOptions<FsStateConfiguration>().Configure(cfg =>
            {
                string? fileName = configuration["FS_STATE_FILE"];
                if (!string.IsNullOrWhiteSpace(fileName))
                {
                    cfg.File = fileName;
                }
            });
            services.AddTransient<IStateRepository, FsStateRepository>();
        }
    }
}