using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Cdc.Kafka.Producer;
using Cdc.Outbox;
using Cdc.Service;
using Cdc.Service.Exceptions;
using Confluent.Kafka;
using Google.Protobuf.WellKnownTypes;
using Microsoft.Extensions.Options;
using Timestamp = Google.Protobuf.WellKnownTypes.Timestamp;

namespace Cdc.Kafka
{
    internal class KafkaTargetRepository : ITargetRepository
    {
        private readonly IProducer<string, OutboxEntry> producer;
        private readonly KafkaCdcConfiguration kafkaConfiguration;

        public KafkaTargetRepository(
            IProducerFactory producerFactory,
            IOptions<KafkaCdcConfiguration> kafkaConfiguration)
        {
            this.producer = producerFactory.Create<OutboxEntry>();
            this.kafkaConfiguration = kafkaConfiguration.Value;
        }

        public async Task<IEnumerable<IChange>> Publish(IEnumerable<IChange> capturesToPublish, CancellationToken cancellationToken)
        {
            foreach (var capture in capturesToPublish)
            {
                var outboxEntry = new OutboxEntry
                {
                    Id = capture.Id,
                    AggregateId = capture.AggregateId,
                    AggregateType = capture.AggregateType,
                    Payload = Any.Parser.ParseFrom(capture.Payload),
                    EventType = capture.EventType,
                    CreatedAt = Timestamp.FromDateTime(capture.CreatedAt)
                };
                var message = new Message<string, OutboxEntry>
                {
                    Key = capture.AggregateId,
                    Value = outboxEntry
                };
                string topic = kafkaConfiguration.Topic!.Replace("{}", capture.AggregateType);
                try
                {
                    await producer.ProduceAsync(topic, message, cancellationToken);
                }
                catch (ProduceException<string, OutboxEntry> ex)
                {
                    throw new TargetException("Failed to write to Kafka target", ex);
                }
            }
            return capturesToPublish;
        }
    }
}