namespace Cdc.Kafka
{
    internal class KafkaCdcConfiguration
    {
        public string? Topic { get; set; } = "outbox.{}";
    }
}