using System;
using System.Collections.Generic;
using Confluent.Kafka;
using Confluent.SchemaRegistry;
using Confluent.SchemaRegistry.Serdes;
using Google.Protobuf;
using Microsoft.Extensions.Options;

namespace Cdc.Kafka.Producer;
internal class MemoryCachedKafkaProducerFactory : IProducerFactory, IDisposable
{
    private readonly ProducerConfig producerConfig;
    private readonly ISchemaRegistryClient schemaRegistryClient;
    private readonly IDictionary<Type, IClient> producers;

    public MemoryCachedKafkaProducerFactory(
        IOptions<ProducerConfig> producerConfig,
        ISchemaRegistryClient schemaRegistryClient)
    {
        this.producerConfig = producerConfig.Value;
        this.schemaRegistryClient = schemaRegistryClient;
        this.producers = new Dictionary<Type, IClient>();
    }

    public IProducer<string, TMessage> Create<TMessage>() where TMessage : class, IMessage<TMessage>, new()
    {
        IProducer<string, TMessage> producer;
        if (producers.TryGetValue(typeof(TMessage), out IClient? uncastProducer))
        {
            producer = (IProducer<string, TMessage>)uncastProducer!;
        }
        else
        {
            producer = new ProducerBuilder<string, TMessage>(producerConfig)
                .SetValueSerializer(new ProtobufSerializer<TMessage>(schemaRegistryClient))
                .Build();
            producers.Add(typeof(TMessage), producer);
        }
        return producer;
    }

    public void Dispose()
    {
        foreach (var producer in producers.Values)
        {
            producer.Dispose();
        }
    }
}
