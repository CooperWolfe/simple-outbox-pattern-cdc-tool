using Confluent.Kafka;
using Google.Protobuf;

namespace Cdc.Kafka.Producer
{
    public interface IProducerFactory
    {
        /// <summary>
        /// Creates a producer for a particular data shape
        /// </summary>
        /// <typeparam name="TMessage">The type of message to produce, must be GRPC</typeparam>
        IProducer<string, TMessage> Create<TMessage>() where TMessage : class, IMessage<TMessage>, new();
    }
}