using Confluent.Kafka;
using Confluent.SchemaRegistry;
using Microsoft.Extensions.Options;

namespace Cdc.Kafka.Configuration
{
    /// <summary>
    /// Gives access to the options builders necessary for binding and overriding configuration values
    /// </summary>
    public class KafkaProducerConfiguration
    {
        internal KafkaProducerConfiguration(OptionsBuilder<SchemaRegistryConfig> schemaRegistry, OptionsBuilder<ProducerConfig> producer)
        {
            SchemaRegistry = schemaRegistry;
            Producer = producer;
        }

        /// <summary>
        /// Options related to the schema registry
        /// </summary>
        public OptionsBuilder<SchemaRegistryConfig> SchemaRegistry { get; }

        /// <summary>
        /// Options related to the producer itself
        /// </summary>
        public OptionsBuilder<ProducerConfig> Producer { get; }
    }
}