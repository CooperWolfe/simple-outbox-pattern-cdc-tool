using Cdc.Kafka;
using Cdc.Kafka.Configuration;
using Cdc.Kafka.Producer;
using Cdc.Service;
using Confluent.Kafka;
using Confluent.SchemaRegistry;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class KafkaServiceCollectionExtensions
    {
        public static void AddKafka(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddOptions<KafkaCdcConfiguration>().Configure(cfg =>
            {
                string? topic = configuration["KAFKA_TOPIC"];
                if (!string.IsNullOrWhiteSpace(topic))
                {
                    cfg.Topic = topic;
                }
            });

            var schemaRegistryOptionsBuilder = services.AddOptions<SchemaRegistryConfig>();
            var producerOptionsBuilder = services.AddOptions<ProducerConfig>();

            var kafkaProducerConfiguration = new KafkaProducerConfiguration(schemaRegistryOptionsBuilder, producerOptionsBuilder);
            kafkaProducerConfiguration.Producer.Configure(cfg =>
            {
                cfg.BootstrapServers = configuration["KAFKA_BOOTSTRAP_SERVERS"];
                cfg.MessageMaxBytes = int.Parse(configuration["KAFKA_MESSAGE_MAX_BYTES"]);
            });
            kafkaProducerConfiguration.SchemaRegistry.Configure(cfg =>
            {
                cfg.Url = configuration["KAFKA_SCHEMA_REGISTRY_URL"];
            });

            services.AddSingleton<ISchemaRegistryClient>(sp =>
            {
                var schemaRegistryConfig = sp.GetRequiredService<IOptions<SchemaRegistryConfig>>().Value;
                return new CachedSchemaRegistryClient(schemaRegistryConfig);
            });
            services.AddSingleton<IProducerFactory, MemoryCachedKafkaProducerFactory>();

            services.AddTransient<ITargetRepository, KafkaTargetRepository>();
        }
    }
}