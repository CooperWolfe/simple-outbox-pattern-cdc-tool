using System;
using System.Text.Json.Serialization;
using Cdc.Service;

namespace Cdc.Postgres
{
    public class PostgresChange : IChange
    {
        [Obsolete("For deserialization only")]
        public PostgresChange()
        {
            Id = string.Empty;
            AggregateId = string.Empty;
            Payload = new byte[0];
            EventType = string.Empty;
            AggregateType = string.Empty;
        }

        public string Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string AggregateId { get; set; }
        public byte[] Payload { get; set; }
        public string EventType { get; set; }
        public string AggregateType { get; set; }
    }
}