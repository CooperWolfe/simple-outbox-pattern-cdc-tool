using System.Threading;
using Cdc.Postgres.CommandLine;
using Cdc.Service.Listener;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Cdc.Postgres
{
    internal class PostgresListenerFactory : ISourceListenerFactory
    {
        private readonly PostgresConfiguration postgresConfiguration;
        private readonly ILoggerFactory loggerFactory;
        private readonly ICommandLineCommandFactory commandFactory;

        public PostgresListenerFactory(
            IOptions<PostgresConfiguration> postgresConfiguration,
            ILoggerFactory loggerFactory,
            ICommandLineCommandFactory commandFactory)
        {
            this.postgresConfiguration = postgresConfiguration.Value;
            this.loggerFactory = loggerFactory;
            this.commandFactory = commandFactory;
        }

        public ISourceListener Create(ISourceListenerDelegate @delegate)
        {
            return new PostgresListener(
                @delegate,
                postgresConfiguration,
                loggerFactory.CreateLogger<PostgresListener>(),
                commandFactory);
        }
    }
}