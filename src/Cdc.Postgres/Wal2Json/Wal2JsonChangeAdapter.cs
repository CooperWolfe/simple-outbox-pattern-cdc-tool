using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.Json;
using Cdc.Service;

namespace Cdc.Postgres.Wal2Json
{
    internal class Wal2JsonChangeAdapter : IChange
    {
        private readonly Wal2JsonChange change;
        private readonly IDictionary<string, JsonElement> dictionary;

        public Wal2JsonChangeAdapter(Wal2JsonChange change)
        {
            this.change = change;
            dictionary = new Dictionary<string, JsonElement>();
            for (int i = 0; i < change.ColumnNames!.Length; ++i)
            {
                dictionary.Add(change.ColumnNames[i], change.ColumnValues![i]);
            }
        }

        public string Id => dictionary["id"].GetString()!;
        public DateTime CreatedAt => DateTime.Parse(dictionary["created_at"].GetString()!, null, DateTimeStyles.AdjustToUniversal);
        public string AggregateId => dictionary["aggregate_id"].GetString()!;
        public byte[] Payload => ToByteArray(dictionary["payload"].GetString());
        public string EventType => dictionary["event_type"].GetString()!; 
        public string AggregateType => dictionary["aggregate_type"].GetString()!;

        /// <summary>
        /// Linear, no-division hex string to byte array conversion
        /// </summary>
        private byte[] ToByteArray(string? hexString)
        {
            if (hexString == null) return new byte[0];
            byte[] bytes = new byte[hexString.Length / 2];
            for (int i = 0, j = 0; i < hexString.Length - 1; i += 2, j++)
            {
                string byteString = new string(new[] { hexString[i], hexString[i+1] });
                bytes[j] = Convert.ToByte(byteString, 16);
            }
            return bytes;
        }
    }
}