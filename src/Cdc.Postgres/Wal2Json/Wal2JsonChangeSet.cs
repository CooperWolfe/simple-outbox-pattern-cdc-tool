using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using Cdc.Service;

namespace Cdc.Postgres.Wal2Json
{
    internal class Wal2JsonChangeSet
    {
        private IEnumerable<Wal2JsonChange>? change;
        [JsonPropertyName("change")]
        public IEnumerable<Wal2JsonChange>? Change
        {
            get { return change; }
            init { change = value?.Where(x => x.Kind == "insert"); }
        }

        /// <throws cref="Wal2JsonException"></throws>
        public void Validate(PostgresConfiguration sourceConfiguration)
        {
            if (Change == null)
            {
                throw new Wal2JsonException("Unexpectedly found null change");
            }
            foreach (var change in Change)
            {
                change.Validate(sourceConfiguration);
            }
        }

        public IEnumerable<IChange> ToChanges()
        {
            return Change!
                .Select<Wal2JsonChange, IChange>(change => new Wal2JsonChangeAdapter(change));
        }
    }
}