using System;
using System.Runtime.Serialization;
using Cdc.Service.Exceptions;

namespace Cdc.Postgres.Wal2Json
{
    [Serializable]
    public class Wal2JsonException : SourceException
    {
        public Wal2JsonException() { }
        public Wal2JsonException(string message) : base(message) { }
        public Wal2JsonException(string message, Exception inner) : base(message, inner) { }
        protected Wal2JsonException(
            SerializationInfo info,
            StreamingContext context) : base(info, context) { }
    }
}