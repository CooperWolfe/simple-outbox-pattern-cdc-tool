using System.Text.Json;
using System.Text.Json.Serialization;

namespace Cdc.Postgres.Wal2Json
{
    internal class Wal2JsonChange
    {
        [JsonPropertyName("kind")]
        public string? Kind { get; init; }

        [JsonPropertyName("schema")]
        public string? Schema { get; init; }

        [JsonPropertyName("table")]
        public string? Table { get; init; }

        [JsonPropertyName("columnnames")]
        public string[]? ColumnNames { get; init; }

        [JsonPropertyName("columntypes")]
        public string[]? ColumnTypes { get; init; }

        [JsonPropertyName("columnvalues")]
        public JsonElement[]? ColumnValues { get; init; }

        public void Validate(PostgresConfiguration sourceConfiguration)
        {
            if (!string.Equals(Schema, sourceConfiguration.Schema, System.StringComparison.OrdinalIgnoreCase))
            {
                throw new Wal2JsonException($"Unexpected schema encountered: {Schema}");
            }
            if (!string.Equals(Table, sourceConfiguration.Table, System.StringComparison.OrdinalIgnoreCase))
            {
                throw new Wal2JsonException($"Unexpected table encountered: {Table}");
            }
            if (ColumnNames == null)
            {
                throw new Wal2JsonException("Unexpectedly found null column names");
            }
            if (ColumnTypes == null)
            {
                throw new Wal2JsonException("Unexpectedly found null column types");
            }
            if (ColumnValues == null)
            {
                throw new Wal2JsonException("Unexpectedly found null column values");
            }
        }
    }
}