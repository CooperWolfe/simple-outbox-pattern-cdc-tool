using System;
using System.Data;
using Cdc.Postgres;
using Cdc.Postgres.CommandLine;
using Cdc.Service;
using Cdc.Service.Listener;
using Microsoft.Extensions.Options;
using Npgsql;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class PostgresServiceCollectionExtensions
    {
        public static void AddPostgresSource(this IServiceCollection services, Action<OptionsBuilder<PostgresConfiguration>> configure)
        {
            var optionsBuilder = services.AddOptions<PostgresConfiguration>();
            configure(optionsBuilder);

            Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;

            services.AddLogging();
            
            services.AddTransient<IDbConnection>(sp =>
            {
                var postgresConfiguration = sp.GetRequiredService<IOptions<PostgresConfiguration>>().Value;
                var connection = new NpgsqlConnection(postgresConfiguration.ConnectionString);
                connection.Open();
                return connection;
            });
            services.AddTransient<ISourceRepository, PostgresSourceRepository>();
            services.AddTransient<ISourceListenerFactory, PostgresListenerFactory>();
            services.AddTransient<ICommandLineCommandFactory, CommandLineCommandFactory>();
        }
    }
}