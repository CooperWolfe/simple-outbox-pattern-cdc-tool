using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Cdc.Service;
using Cdc.Service.State;
using Npgsql;
using Cdc.Service.Exceptions;
using Microsoft.Extensions.Options;

namespace Cdc.Postgres
{
    internal class PostgresSourceRepository : ISourceRepository
    {
        private readonly IDbConnection connection;
        private readonly PostgresConfiguration postgresConfiguration;

        public PostgresSourceRepository(
            IDbConnection connection,
            IOptions<PostgresConfiguration> postgresConfiguration)
        {
            this.connection = connection;
            this.postgresConfiguration = postgresConfiguration.Value;
        }

        public async Task<IEnumerable<IChange>> ReadAllPending(LastConsumedState? lastConsumed, CancellationToken cancellationToken)
        {
            var query = lastConsumed == null
                ? new CommandDefinition(
                    commandText: @$"
                        SELECT id, aggregate_id, aggregate_type, payload, event_type, created_at
                        FROM {postgresConfiguration.Schema}.{postgresConfiguration.Table}",
                    cancellationToken: cancellationToken)
                : new CommandDefinition(
                    commandText: @$"
                        SELECT id, aggregate_id, aggregate_type, payload, event_type, created_at
                        FROM {postgresConfiguration.Schema}.{postgresConfiguration.Table}
                        WHERE created_at >= @CreatedAt
                            AND id != ANY(@Ids)",
                    parameters: lastConsumed,
                    cancellationToken: cancellationToken);

            try
            {
                return await connection.QueryAsync<PostgresChange>(query);
            }
            catch (NpgsqlException ex)
            {
                throw new SourceException("An error occurred while reading pending messages", ex);
            }
        }
    }
}