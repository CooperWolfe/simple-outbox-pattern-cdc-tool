using System.Threading;
using Microsoft.Extensions.Logging;

namespace Cdc.Postgres.CommandLine
{
    internal class CommandLineCommandFactory : ICommandLineCommandFactory
    {
        private readonly ILoggerFactory loggerFactory;

        public CommandLineCommandFactory(ILoggerFactory loggerFactory)
        {
            this.loggerFactory = loggerFactory;
        }

        public CommandLineCommand Create(string command, string args, CancellationToken cancellationToken)
        {
            return new CommandLineCommand(command, args, cancellationToken);
        }
    }
}