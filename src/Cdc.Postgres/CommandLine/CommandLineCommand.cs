using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Cdc.Postgres.CommandLine
{
    internal class CommandLineCommand : IDisposable
    {
        private readonly object mutex = new object();
        private readonly Process process;
        private readonly CancellationToken cancellationToken;
        private readonly CancellationTokenRegistration cancellationRegistration;
        private readonly TaskCompletionSource commandCompletion;

        public CommandLineCommand(string command, string args, CancellationToken cancellationToken)
        {
            commandCompletion = new TaskCompletionSource();
            process = new Process
            {
                StartInfo =
                {
                    FileName = command,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    RedirectStandardInput = true,
                    Arguments = args,
                    UseShellExecute = false,
                    CreateNoWindow = true
                },
                EnableRaisingEvents = true
            };
            process.OutputDataReceived += OnStdOut;
            process.ErrorDataReceived += OnStdErr;
            process.Exited += (_, _) =>
            {
                if (process.ExitCode == 0)
                {
                    commandCompletion.TrySetResult();
                }
                else
                {
                    commandCompletion.TrySetException(new CommandLineException($"Process exited with code {process.ExitCode}"));
                }
            };
            this.cancellationToken = cancellationToken;
            cancellationRegistration = cancellationToken.Register(() => commandCompletion.TrySetCanceled());
        }

        public event OnCommandLineOutput? OnOutput;
        public event OnCommandLineOutput? OnError;

        public async Task Execute()
        {
            if (!process.Start())
            {
                throw new CommandLineException("Could not start command process");
            }
            
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();

            await commandCompletion.Task;
        }

        private void OnStdOut(object sender, DataReceivedEventArgs @event)
        {
            lock (mutex)
            {
                if (OnOutput != null)
                {
                    try
                    {
                        OnOutput.Invoke(@event.Data, cancellationToken).Wait();
                    }
                    catch (Exception ex)
                    {
                        commandCompletion.TrySetException(ex);
                    }
                }
            }
        }

        private void OnStdErr(object sender, DataReceivedEventArgs @event)
        {
            lock (mutex)
            {
                if (OnError != null)
                {
                    try
                    {
                        OnError.Invoke(@event.Data, cancellationToken).Wait();
                    }
                    catch (Exception ex)
                    {
                        commandCompletion.TrySetException(ex);
                    }
                }
            }
        }

        public void Dispose()
        {
            try
            {
                if (!process.HasExited)
                {
                    process.Kill();
                }
            }
            catch (InvalidOperationException) // Process has exited
            {
            }
            finally
            {
                process.Dispose();
                cancellationRegistration.Dispose();
            }
        }
    }
}