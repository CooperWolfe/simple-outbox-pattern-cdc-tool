using System.Threading;

namespace Cdc.Postgres.CommandLine
{
    internal interface ICommandLineCommandFactory
    {
        CommandLineCommand Create(string command, string args, CancellationToken cancellationToken);
    }
}