using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Cdc.Postgres.CommandLine
{
    internal delegate Task OnCommandLineOutput(string? outputLine, CancellationToken cancellationToken);
}