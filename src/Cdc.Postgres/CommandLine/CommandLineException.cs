namespace Cdc.Postgres.CommandLine
{
    [System.Serializable]
    internal class CommandLineException : System.Exception
    {
        public CommandLineException() { }
        public CommandLineException(string message) : base(message) { }
        public CommandLineException(string message, System.Exception inner) : base(message, inner) { }
        protected CommandLineException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}