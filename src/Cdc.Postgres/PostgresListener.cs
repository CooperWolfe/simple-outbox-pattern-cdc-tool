using System;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Cdc.Postgres.CommandLine;
using Cdc.Postgres.Wal2Json;
using Cdc.Service.Exceptions;
using Cdc.Service.Listener;
using Cdc.Service.State;
using Microsoft.Extensions.Logging;

namespace Cdc.Postgres
{
    internal class PostgresListener : ISourceListener
    {
        private bool hasCalledListen = false;

        private readonly ISourceListenerDelegate @delegate;
        private readonly PostgresConfiguration postgresConfiguration;
        private readonly ILogger<PostgresListener> logger;
        private readonly ICommandLineCommandFactory commandFactory;

        public PostgresListener(
            ISourceListenerDelegate @delegate,
            PostgresConfiguration postgresConfiguration,
            ILogger<PostgresListener> logger,
            ICommandLineCommandFactory commandFactory)
        {
            this.@delegate = @delegate;
            this.postgresConfiguration = postgresConfiguration;
            this.logger = logger;
            this.commandFactory = commandFactory;
        }

        public async Task Listen(LastConsumedState? lastConsumed, CancellationToken cancellationToken)
        {
            hasCalledListen = hasCalledListen ? throw new InvalidOperationException() : true;

            logger.LogInformation("Setting up .pgpass");
            await SetupPgpass(cancellationToken);

            using (var pgrecvlogical = CreatePgrecvlogicalCommand(cancellationToken))
            {
                logger.LogInformation("Starting pg_recvlogical");
                try
                {
                    await pgrecvlogical.Execute();
                }
                catch (CommandLineException ex)
                {
                    throw new SourceException("Failed while executing pg_recvlogical", ex);
                }
            }

            logger.LogInformation("Done listening");
        }

        private async Task SetupPgpass(CancellationToken cancellationToken)
        {
            await using (StreamWriter writer = File.CreateText(postgresConfiguration.PgpassLocation!))
            {
                await writer.WriteLineAsync(postgresConfiguration.Pgpass);
                await writer.DisposeAsync();
            }

            using (CommandLineCommand updatePgpassPermissionsCommand = CreateUpdatePgpassPermissionsCommand(cancellationToken))
            {
                await updatePgpassPermissionsCommand.Execute();
            }
        }

        private async Task OnOutput(string? line, CancellationToken cancellationToken)
        {
            if (line == null) return;
            Wal2JsonChangeSet wal2jsonChangeSet;
            try
            {
                wal2jsonChangeSet = JsonSerializer.Deserialize<Wal2JsonChangeSet>(line)!;
            }
            catch (JsonException)
            {
                logger.LogWarning(line);
                return;
            }

            wal2jsonChangeSet!.Validate(postgresConfiguration);
            await @delegate.OnTrigger(wal2jsonChangeSet.ToChanges(), cancellationToken);
        }

        private Task OnError(string? line, CancellationToken cancellationToken)
        {
            if (line != null)
            {
                logger.LogError(line);
            }
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            // Nothing to do
        }

        private CommandLineCommand CreateUpdatePgpassPermissionsCommand(CancellationToken cancellationToken)
        {
            return commandFactory.Create("chmod", $"600 {postgresConfiguration.PgpassLocation}", cancellationToken);
        }

        private CommandLineCommand CreatePgrecvlogicalCommand(CancellationToken cancellationToken)
        {
            string args = new StringBuilder()
                .Append($"-h {postgresConfiguration.Host} ")
                .Append($"-p {postgresConfiguration.Port} ")
                .Append($"-U {postgresConfiguration.User} ")
                .Append($"-d {postgresConfiguration.Database} ")
                .Append($"-S {postgresConfiguration.Slot} ")
                .Append("--create-slot --if-not-exists -P wal2json ")
                .Append($"--start -o add-tables={postgresConfiguration.Schema}.{postgresConfiguration.Table} -f -")
                .ToString();
            var command = commandFactory.Create("pg_recvlogical", args, cancellationToken);
            command.OnOutput += OnOutput;
            command.OnError += OnError;
            return command;
        }
    }
}