namespace Cdc.Postgres
{
    public class PostgresConfiguration
    {
        public string? Table { get; set; }
        public string? Schema { get; set; }
        public string? Slot { get; set; }
        public string? Host { get; set; }
        public string? Port { get; set; }
        public string? User { get; set; }
        public string? Password { get; set; }
        public string? Database { get; set; }
        public string? PgpassLocation { get; set; }

        internal string ConnectionString => $"User ID={User};Password={Password};Host={Host};Port={Port};Database={Database}";
        internal string Pgpass => $"{Host}:{Port}:*:{User}:{Password}";
    }
}