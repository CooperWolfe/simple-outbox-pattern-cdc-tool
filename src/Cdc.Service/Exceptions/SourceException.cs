using System;
using System.Runtime.Serialization;

namespace Cdc.Service.Exceptions
{
    [Serializable]
    public class SourceException : CdcException
    {
        public SourceException() { }
        public SourceException(string message) : base(message) { }
        public SourceException(string message, Exception inner) : base(message, inner) { }
        protected SourceException(
            SerializationInfo info,
            StreamingContext context) : base(info, context) { }
    }
}