using System;
using System.Runtime.Serialization;

namespace Cdc.Service.Exceptions
{
    [Serializable]
    public class CdcException : System.Exception
    {
        public CdcException() { }
        public CdcException(string message) : base(message) { }
        public CdcException(string message, Exception inner) : base(message, inner) { }
        protected CdcException(
            SerializationInfo info,
            StreamingContext context) : base(info, context) { }
    }
}