using System;
using System.Runtime.Serialization;

namespace Cdc.Service.Exceptions
{
    [Serializable]
    public class StateException : CdcException
    {
        public StateException() { }
        public StateException(string message) : base(message) { }
        public StateException(string message, Exception inner) : base(message, inner) { }
        protected StateException(
            SerializationInfo info,
            StreamingContext context) : base(info, context) { }
    }
}