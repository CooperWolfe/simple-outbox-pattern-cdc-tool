using System;
using System.Runtime.Serialization;

namespace Cdc.Service.Exceptions
{
    [Serializable]
    public class TargetException : CdcException
    {
        public TargetException() { }
        public TargetException(string message) : base(message) { }
        public TargetException(string message, Exception inner) : base(message, inner) { }
        protected TargetException(
            SerializationInfo info,
            StreamingContext context) : base(info, context) { }
    }
}