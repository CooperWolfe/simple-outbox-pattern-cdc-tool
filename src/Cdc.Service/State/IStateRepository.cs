using System.Threading;
using System.Threading.Tasks;
using Cdc.Service.Exceptions;
using System;

namespace Cdc.Service.State
{
    public interface IStateRepository
    {
        /// <throws><see cref="StateException"/></throws>
        Task<LastConsumedState?> LoadLastConsumed(CancellationToken stoppingToken);

        /// <throws><see cref="StateException"/></throws>
        Task UpdateLastConsumed(LastConsumedState lastConsumedState, CancellationToken stoppingToken);

        /// <throws><see cref="InvalidOperationException"/></throws>
        LastConsumedState? ReadCachedLastConsumed();
    }
}