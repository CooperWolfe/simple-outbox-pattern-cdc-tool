using System;
using System.Collections.Generic;

namespace Cdc.Service.State
{
    public record LastConsumedState(
        IEnumerable<string> Ids,
        DateTime CreatedAt) {}
}