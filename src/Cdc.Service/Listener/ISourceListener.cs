using System;
using System.Threading;
using System.Threading.Tasks;
using Cdc.Service.Exceptions;
using Cdc.Service.State;

namespace Cdc.Service.Listener
{
    public interface ISourceListener : IDisposable
    {
        /// <throws><see cref="SourceException"/></throws>
        Task Listen(LastConsumedState? lastConsumedState, CancellationToken cancellationToken);
    }
}