namespace Cdc.Service.Listener
{
    public interface ISourceListenerFactory
    {
        ISourceListener Create(ISourceListenerDelegate @delegate);
    }
}