using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Cdc.Service.Exceptions;
using Cdc.Service.State;

namespace Cdc.Service.Listener
{
    public interface ISourceListenerDelegate
    {
        /// <param name="captures">The captured changes, ordered by CreatedAt, ascending</param>
        /// <param name="cancellationToken"></param>
        /// <throws><see cref="TargetException"/></throws>
        /// <throws><see cref="StateException"/></throws>
        Task<LastConsumedState?> OnTrigger(IEnumerable<IChange> captures, CancellationToken cancellationToken);
    }
}