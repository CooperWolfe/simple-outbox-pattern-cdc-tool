using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Cdc.Service.State;
using Cdc.Service.Exceptions;

namespace Cdc.Service
{
    public interface ISourceRepository
    {
        /// <throws><see cref="SourceException"/></throws>
        Task<IEnumerable<IChange>> ReadAllPending(LastConsumedState? lastConsumed, CancellationToken stoppingToken);
    }
}