using System.Threading;
using System.Threading.Tasks;

namespace Cdc.Service
{
    public interface ICdcService
    {
        Task GetCurrent(CancellationToken stoppingToken);
        Task Listen(CancellationToken stoppingToken);
    }
}