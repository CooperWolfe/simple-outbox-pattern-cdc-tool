using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Cdc.Service.Exceptions;

namespace Cdc.Service
{
    public interface ITargetRepository
    {
        /// <throws><see cref="TargetException"/></throws>
        Task<IEnumerable<IChange>> Publish(IEnumerable<IChange> capturesToPublish, CancellationToken stoppingToken);
    }
}