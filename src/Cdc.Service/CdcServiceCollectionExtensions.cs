using Cdc.Service;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class CdcServiceCollectionExtensions
    {
        public static void AddCdc(this IServiceCollection services)
        {
            services.AddLogging();
            services.AddTransient<ICdcService, CdcService>();
        }
    }
}