using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Cdc.Service.Listener;
using Cdc.Service.State;
using Microsoft.Extensions.Logging;

namespace Cdc.Service
{
    internal class CdcService : ICdcService, ISourceListenerDelegate
    {
        private readonly ILogger<CdcService> logger;
        private readonly IStateRepository stateRepository;
        private readonly ISourceRepository sourceRepository;
        private readonly ITargetRepository targetRepository;
        private readonly ISourceListenerFactory sourceListenerFactory;

        public CdcService(
            ILogger<CdcService> logger,
            IStateRepository stateRepository,
            ISourceRepository sourceRepository,
            ITargetRepository targetRepository,
            ISourceListenerFactory sourceListenerFactory)
        {
            this.logger = logger;
            this.stateRepository = stateRepository;
            this.sourceRepository = sourceRepository;
            this.targetRepository = targetRepository;
            this.sourceListenerFactory = sourceListenerFactory;
        }

        public async Task GetCurrent(CancellationToken cancellationToken)
        {
            logger.LogInformation("Loading state of last consumption");
            var lastConsumed = await stateRepository.LoadLastConsumed(cancellationToken);

            logger.LogInformation("Reading pending changes");
            var capturesToPublish = await sourceRepository.ReadAllPending(lastConsumed, cancellationToken);

            if (!capturesToPublish.Any())
            {
                logger.LogInformation("No pending changes");
                return;
            }
            
            logger.LogInformation($"{capturesToPublish.Count()} pending changes found. Publishing.");
            var publishedCaptures = await targetRepository.Publish(capturesToPublish, cancellationToken);

            logger.LogDebug("Updating consumption state");
            await stateRepository.UpdateLastConsumed(ConvertToLastConsumed(publishedCaptures), cancellationToken);
        }

        public async Task Listen(CancellationToken cancellationToken)
        {
            using (var sourceListener = sourceListenerFactory.Create(this))
            {
                logger.LogInformation("Listening for changes");
                var lastConsumed = stateRepository.ReadCachedLastConsumed();
                await sourceListener.Listen(lastConsumed, cancellationToken);
            }
        }

        public async Task<LastConsumedState?> OnTrigger(IEnumerable<IChange> captures, CancellationToken cancellationToken)
        {
            var lastConsumed = stateRepository.ReadCachedLastConsumed();
            int captureCount = captures.Count();
            if (captureCount < 1) return lastConsumed;

            var capturesToPublish = captures.Where(capture =>
                lastConsumed == null
                || capture.CreatedAt >= lastConsumed.CreatedAt 
                    && !lastConsumed.Ids.Contains(capture.Id));
            int publishCount = capturesToPublish.Count();
            if (publishCount == 0)
            {
                logger.LogDebug($"Ignored {captureCount} changes.");
                return lastConsumed;
            }

            logger.LogDebug(Summarize(captureCount, publishCount));
            var publishedCaptures = await targetRepository.Publish(capturesToPublish, cancellationToken);

            logger.LogDebug("Updating consumption state");
            lastConsumed = ConvertToLastConsumed(publishedCaptures);
            await stateRepository.UpdateLastConsumed(lastConsumed, cancellationToken);
            return lastConsumed;
        }

        /// <param name="publishedCaptures">Published changes, in order of CreatedAt, ascending</param>
        private static LastConsumedState ConvertToLastConsumed(IEnumerable<IChange> publishedCaptures)
        {
            var lastCreatedDate = publishedCaptures.Last().CreatedAt;
            var lastCaptures = publishedCaptures.Where(capture => capture.CreatedAt == lastCreatedDate);
            return new LastConsumedState(
                Ids: lastCaptures.Select(capture => capture.Id).ToArray(),
                CreatedAt: lastCreatedDate);
        }

        private static string Summarize(int captureCount, int publishCount)
        {
            int ignoreCount = captureCount - publishCount;
            return new StringBuilder($"Captured {captureCount} changes")
                .Append(ignoreCount > 0 ? $" and ignored {ignoreCount}." : ".")
                .Append(" Publishing.")
                .ToString();
        }
    }
}