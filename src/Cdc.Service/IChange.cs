using System;

namespace Cdc.Service
{
    public interface IChange
    {
        string Id { get; }
        DateTime CreatedAt { get; }
        string AggregateId { get; }
        byte[] Payload { get; }
        string EventType { get; }
        string AggregateType { get; }
    }
}