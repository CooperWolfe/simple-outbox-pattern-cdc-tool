using Cdc.Service;
using Cdc.Service.Exceptions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Cdc.Worker
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddLogging();
                    switch (hostContext.Configuration["SOURCE"])
                    {
                        case nameof(Sources.Postgres):
                            services.AddPostgresSource(opt =>
                            {
                                opt.Configure(cfg =>
                                {
                                    cfg.Host = hostContext.Configuration["POSTGRES_HOST"];
                                    cfg.Port = hostContext.Configuration["POSTGRES_PORT"];
                                    cfg.User = hostContext.Configuration["POSTGRES_USER"];
                                    cfg.Password = hostContext.Configuration["POSTGRES_PASSWORD"];
                                    cfg.Database = hostContext.Configuration["POSTGRES_DATABASE"];
                                    cfg.Schema = hostContext.Configuration["POSTGRES_SCHEMA"];
                                    cfg.Table = hostContext.Configuration["POSTGRES_TABLE"];
                                    cfg.Slot = hostContext.Configuration["POSTGRES_SLOT"];
                                    cfg.PgpassLocation = hostContext.Configuration["POSTGRES_PGPASS"] ?? DefaultPgpassLocation;
                                });
                            });
                            break;
                        case nameof(Sources.Neo4j):
                            services.AddNeo4jSource(opt =>
                            {
                                opt.Configure(cfg =>
                                {
                                    cfg.Host = hostContext.Configuration["NEO4J_HOST"];
                                    cfg.Port = hostContext.Configuration["NEO4J_PORT"];
                                    cfg.Username = hostContext.Configuration["NEO4J_USER"];
                                    cfg.Password = hostContext.Configuration["NEO4J_PASSWORD"];
                                    cfg.Database = hostContext.Configuration["NEO4J_DATABASE"];
                                    cfg.OutboxLabel = hostContext.Configuration["NEO4J_OUTBOX_LABEL"];
                                    cfg.PollTimeoutMs = int.Parse(hostContext.Configuration["NEO4J_POLL_TIMEOUT_MS"]);
                                });
                            });
                            break;
                        case nameof(Sources.Mongo):
                            services.AddMongoSource(opt =>
                            {
                                opt.Configure(cfg =>
                                {
                                    cfg.Host = hostContext.Configuration["MONGO_HOST"];
                                    cfg.Port = int.Parse(hostContext.Configuration["MONGO_PORT"]);
                                    cfg.Username = hostContext.Configuration["MONGO_USERNAME"];
                                    cfg.Password = hostContext.Configuration["MONGO_PASSWORD"];
                                    cfg.Database = hostContext.Configuration["MONGO_DATABASE"];
                                    cfg.OutboxCollection = hostContext.Configuration["MONGO_OUTBOX_COLLECTION"];
                                });
                            });
                            break;
                        default: throw new ConfigurationException($"Invalid source: {hostContext.Configuration["SOURCE"]}");
                    }
                    switch (hostContext.Configuration["TARGET"])
                    {
                        case nameof(Targets.Kafka):
                            services.AddKafka(hostContext.Configuration);
                            break;
                        default: throw new ConfigurationException($"Invalid target: {hostContext.Configuration["TARGET"]}");
                    }
                    switch (hostContext.Configuration["STATE"])
                    {
                        case nameof(States.Fs):
                            services.AddFs(hostContext.Configuration);
                            break;
                        default: throw new ConfigurationException($"Invalid state: {hostContext.Configuration["STATE"]}");
                    }
                    services.AddCdc();

                    services.AddHostedService<CdcWorker>();
                });
    }
}
