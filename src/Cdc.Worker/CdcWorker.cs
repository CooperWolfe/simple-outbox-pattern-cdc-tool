using System;
using System.Threading;
using System.Threading.Tasks;
using Cdc.Service;
using Cdc.Service.Exceptions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Cdc.Worker
{
    internal class CdcWorker : BackgroundService
    {
        private const int DefaultRetryTimeoutMs = 1_000;
        private const int MaxRetryTimeoutMs = 30_000;

        private readonly ILogger<CdcWorker> logger;
        private readonly ICdcService cdcService;
        private readonly IHostApplicationLifetime lifetime;

        public CdcWorker(
            ILogger<CdcWorker> logger,
            ICdcService cdcService,
            IHostApplicationLifetime lifetime)
        {
            this.logger = logger;
            this.cdcService = cdcService;
            this.lifetime = lifetime;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                await DoCdc(stoppingToken);
            }
            catch (TaskCanceledException)
            {
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Application failed");
                lifetime.StopApplication();
            }
        }

        private async Task DoCdc(CancellationToken stoppingToken)
        {
            int retryTimeout = DefaultRetryTimeoutMs;
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    logger.LogInformation("Getting current on changes");
                    await cdcService.GetCurrent(stoppingToken);
                }
                catch (CdcException ex)
                {
                    logger.LogError(ex, $"Failed to get current changes. Retrying in {retryTimeout}ms");
                    await Task.Delay(retryTimeout, stoppingToken);
                    retryTimeout = IncrementTimeout(retryTimeout);
                    continue;
                }

                retryTimeout = DefaultRetryTimeoutMs;

                try
                {
                    logger.LogInformation("Listening for more changes");
                    await cdcService.Listen(stoppingToken); // Should block
                }
                catch (CdcException ex)
                {
                    logger.LogError(ex, $"Failed to listen for future changes. Retrying in {retryTimeout}ms");
                    await Task.Delay(retryTimeout, stoppingToken);
                    continue;
                }
            }
        }

        private static int IncrementTimeout(int retryTimeout)
        {
            int newTimeout = retryTimeout * 2;
            return newTimeout > MaxRetryTimeoutMs ? MaxRetryTimeoutMs : newTimeout;
        }
    }
}
