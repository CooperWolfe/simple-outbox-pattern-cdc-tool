using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Cdc.Service;

namespace Cdc.UnitTests.Fakes
{
    public class FakeTargetRepository : ITargetRepository
    {
        public int TimesPublishCalled { get; private set; } = 0;

        public Task<IEnumerable<IChange>> Publish(IEnumerable<IChange> capturesToPublish, CancellationToken stoppingToken)
        {
            TimesPublishCalled++;
            return Task.FromResult(capturesToPublish);
        }
    }
}