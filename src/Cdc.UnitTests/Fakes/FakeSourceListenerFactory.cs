using Cdc.Service.Listener;

namespace Cdc.UnitTests.Fakes
{
    public class FakeSourceListenerFactory : ISourceListenerFactory
    {
        public ISourceListener Create(ISourceListenerDelegate @delegate)
        {
            return new FakeSourceListener();
        }
    }
}