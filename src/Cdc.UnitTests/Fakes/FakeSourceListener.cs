using System.Threading;
using System.Threading.Tasks;
using Cdc.Service.Listener;
using Cdc.Service.State;

namespace Cdc.UnitTests.Fakes
{
    public class FakeSourceListener : ISourceListener
    {
        public Task Listen(LastConsumedState? lastConsumed, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public void Dispose()
        {
        }
    }
}