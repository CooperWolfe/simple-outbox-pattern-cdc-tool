using System.Threading;
using System.Threading.Tasks;
using Cdc.Service.State;

namespace Cdc.UnitTests.Fakes
{
    public class FakeStateRepository : IStateRepository
    {
        public FakeStateRepository(LastConsumedState? state = null)
        {
            LastConsumedState = state;
        }

        public LastConsumedState? LastConsumedState { get; private set; }

        public Task<LastConsumedState?> LoadLastConsumed(CancellationToken stoppingToken)
        {
            return Task.FromResult(LastConsumedState);
        }

        public LastConsumedState? ReadCachedLastConsumed()
        {
            return LastConsumedState;
        }

        public Task UpdateLastConsumed(LastConsumedState lastConsumedState, CancellationToken stoppingToken)
        {
            LastConsumedState = lastConsumedState;
            return Task.CompletedTask;
        }
    }
}