using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Cdc.Service;
using Cdc.Service.State;

namespace Cdc.UnitTests.Fakes
{
    public class FakeSourceRepository : ISourceRepository
    {
        private readonly IEnumerable<IChange> changes;

        public FakeSourceRepository(IEnumerable<IChange>? changes = null)
        {
            this.changes = changes ?? Enumerable.Empty<IChange>();
        }

        public Task<IEnumerable<IChange>> ReadAllPending(LastConsumedState? lastConsumed, CancellationToken stoppingToken)
        {
            return Task.FromResult(changes);
        }
    }
}