using System;
using Cdc.Service;

namespace Cdc.UnitTests.Fakes
{
    public class FakeChange : IChange
    {
        public FakeChange(string id, DateTime createdAt)
        {
            Id = id;
            CreatedAt = createdAt;
            AggregateId = "abc123";
            Payload = new byte[0];
            EventType = "something.happened";
            AggregateType = "something";
        }

        public string Id { get; }
        public DateTime CreatedAt { get; }

        public string AggregateId { get; }
        public byte[] Payload { get; }
        public string EventType { get; }
        public string AggregateType { get; }
    }
}