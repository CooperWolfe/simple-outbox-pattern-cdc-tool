using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cdc.Service;
using Cdc.Service.State;
using Cdc.UnitTests.Fakes;
using Xunit;

namespace Cdc.UnitTests
{
    public class CdcServiceTests
    {
        [Fact]
        public async Task GetCurrent_NoChangesToGetCurrentOn_DoesNotPublishAnyChanges()
        {
            var stubSourceRepository = new FakeSourceRepository(changes: Enumerable.Empty<IChange>());
            var mockTargetRepository = new FakeTargetRepository();
            var service = CreateService(
                sourceRepository: stubSourceRepository,
                targetRepository: mockTargetRepository);

            await service.GetCurrent(default);

            Assert.Equal(0, mockTargetRepository.TimesPublishCalled);
        }

        private class NoChangesToPublish : IEnumerable<object?[]>
        {
            public IEnumerator<object?[]> GetEnumerator()
            {
                yield return new object?[]
                {
                    null,
                    new IChange[0]
                };
                yield return new object?[]
                {
                    new LastConsumedState(
                        Ids: new[] { "???" },
                        CreatedAt: new DateTime(2021, 12, 6)),
                    new IChange[]
                    {
                        new FakeChange(id: "a", createdAt: new DateTime(2021, 12, 5))
                    }
                };
                yield return new object?[]
                {
                    new LastConsumedState(
                        Ids: new[] { "a" },
                        CreatedAt: new DateTime(2021, 12, 5)),
                    new IChange[]
                    {
                        new FakeChange(id: "a", createdAt: new DateTime(2021, 12, 5))
                    }
                };
            }
            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }
        [Theory]
        [ClassData(typeof(NoChangesToPublish))]
        public async Task OnTrigger_NoChangesToPublish_PublishesNoChanges(LastConsumedState? state, IEnumerable<IChange> captures)
        {
            var stubStateRepository = new FakeStateRepository(state);
            var mockTargetRepository = new FakeTargetRepository();
            var service = CreateService(
                stateRepository: stubStateRepository,
                targetRepository: mockTargetRepository);

            await service.OnTrigger(captures, default);

            Assert.Equal(0, mockTargetRepository.TimesPublishCalled);
        }

        private class UpdatedLastConsumedStates : IEnumerable<object?[]>
        {
            public IEnumerator<object?[]> GetEnumerator()
            {
                yield return new object[]
                {
                    new IChange[]
                    {
                        new FakeChange("a", new DateTime(2021, 12, 5)),
                        new FakeChange("b", new DateTime(2021, 12, 6)),
                        new FakeChange("c", new DateTime(2021, 12, 7))
                    },
                    new LastConsumedState(Ids: new[] { "c" }, CreatedAt: new DateTime(2021, 12, 7))
                };
                yield return new object[]
                {
                    new IChange[]
                    {
                        new FakeChange("a", new DateTime(2021, 12, 5)),
                        new FakeChange("b", new DateTime(2021, 12, 6)),
                        new FakeChange("c", new DateTime(2021, 12, 6))
                    },
                    new LastConsumedState(Ids: new[] { "b", "c" }, CreatedAt: new DateTime(2021, 12, 6))
                };
            }
            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }
        [Theory]
        [ClassData(typeof(UpdatedLastConsumedStates))]
        public async Task OnTrigger_LastConsumedUpdatedCorrectly(IEnumerable<IChange> captures, LastConsumedState expectedLastConsumed)
        {
            var mockStateRepository = new FakeStateRepository(state: null);
            var service = CreateService(stateRepository: mockStateRepository);

            await service.OnTrigger(captures, default);

            Assert.Equal(expectedLastConsumed.Ids, mockStateRepository.LastConsumedState!.Ids);
            Assert.Equal(expectedLastConsumed.CreatedAt, mockStateRepository.LastConsumedState!.CreatedAt);
        }

        private static CdcService CreateService(
            IStateRepository? stateRepository = null,
            ISourceRepository? sourceRepository = null,
            ITargetRepository? targetRepository = null)
        {
            return new CdcService(
                logger: new FakeLogger<CdcService>(),
                stateRepository: stateRepository ?? new FakeStateRepository(),
                sourceRepository: sourceRepository ?? new FakeSourceRepository(),
                targetRepository: targetRepository ?? new FakeTargetRepository(),
                sourceListenerFactory: new FakeSourceListenerFactory());
        }
    }
}